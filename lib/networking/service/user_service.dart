import 'package:centratamagroup/core/constant.dart';
import 'package:centratamagroup/model/pokemon_detail_model.dart';
import 'package:centratamagroup/model/pokemon_model.dart';
import 'package:flutter/material.dart';
import 'base_service.dart';

import '../../main.dart';
import 'base_service.dart';

class UserService extends BaseService {
  Future<PokemonModel> getPokemonList(String ext) async {
    return await get('$ext');
  }

  Future<PokemonDetailModel> getPokemonDetail(String ext) async {
    return await get('${Constant.BASE_API}pokemon/$ext');
  }
}
