import 'dart:convert';

import 'package:centratamagroup/core/constant.dart';
import 'package:centratamagroup/model/pokemon_detail_model.dart';
import 'package:centratamagroup/model/pokemon_model.dart';
import 'package:dio/dio.dart';
import 'package:dio_retry/dio_retry.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BaseService {
//  var _dashboardStores = locator<DashboardStores>();
  var _defaultError = {
    'status': Constant.ERROR,
    'message': Constant.ERROR_MESSAGE
  };

  Future<Map<String, String>> getHeaders() async {
    var prefs = await SharedPreferences.getInstance();
    var maps = Map<String, String>();
    maps['Authorization'] = 'Bearer ${prefs.getString(Constant.TOKEN)}';
    return maps;
  }

//  _logout(BuildContext context) {
//    showDialog(
//        context: context,
//        child: DialogError(
//          error: 'Session expired, please login again.',
//          button: 'Login',
//          buttonClick: () => Navigator.pop(context),
//        )).then((_) {
//      SharedPreferences.getInstance().then((value) {
//        _dashboardStores.resetData();
//        value.setString(Constant.LANGUANGE_CODE, Constant.INDONESIAN);
//        value.setBool(Constant.IS_LOGIN, false);
//
//        Navigator.pushAndRemoveUntil(context,
//            MaterialPageRoute(builder: (_) => LoginView()), (route) => false);
//      });
//    });
//  }

  /// POST WITH FORM DATA
  Future<T> postFormData<T>(
      BuildContext context, String url, FormData body) async {
    T resultResponse;

    try {
      final dio = Dio();
      dio.interceptors.add(LogInterceptor(responseBody: true));
      var response = await dio.post(url,
          data: body,
          options: Options(headers: await getHeaders(), sendTimeout: 5000));
      resultResponse = fromJson<T>(response.data);
    } on DioError catch (e) {
      final statusCode = e?.response?.statusCode ?? 404;

      if (statusCode == 401) {
//        _logout(context);
      } else {
        resultResponse = fromJson<T>(
            e?.response?.data.toString().contains('status')
                ? e?.response?.data
                : _defaultError);
      }
    }

    return resultResponse;
  }

  /// POST
  Future<T> post<T>(BuildContext context, String url) async {
    T resultResponse;

    try {
      final dio = Dio();
      dio.interceptors.add(LogInterceptor(responseBody: true));
      var response = await dio.post(url,
          options: Options(headers: await getHeaders(), sendTimeout: 5000));
      resultResponse = fromJson<T>(response.data);
    } on DioError catch (e) {
      final statusCode = e?.response?.statusCode ?? 404;

      if (statusCode == 401) {
//        _logout(context);
      } else {
        resultResponse = fromJson<T>(
            e?.response?.data.toString().contains('status')
                ? e?.response?.data
                : _defaultError);
      }
    }

    return resultResponse;
  }

  /// POST WITH JSON BODY
  Future<T> postJsonBody<T>(
      BuildContext context, String url, dynamic body) async {
    T resultResponse;

    try {
      final dio = Dio();
      dio.interceptors.add(LogInterceptor(responseBody: true));
      debugPrint("body parameter: ${body.toString()}");
      var response = await dio.post(url,
          data: body,
          options: Options(headers: await getHeaders(), sendTimeout: 5000));
      resultResponse = fromJson<T>(response.data);
    } on DioError catch (e) {
      final statusCode = e?.response?.statusCode ?? 404;

      if (statusCode == 401) {
//        _logout(context);
      } else {
        resultResponse = fromJson<T>(
            e?.response?.data.toString().contains('status')
                ? e?.response?.data
                : _defaultError);
      }
    }

    return resultResponse;
  }

  /// GET WITH QUERY
  Future<T> get<T>(String url) async {
    T resultResponse;

    try {
      final dio = Dio();
      dio.interceptors.add(LogInterceptor(responseBody: true));
      dio.interceptors.add(RetryInterceptor(
        dio: dio,
        logger: Logger("Retry"),
        options: const RetryOptions(
            retryInterval: const Duration(seconds: 1), retries: 3),
      ));
      var response = await dio.get(url,
          options: Options(headers: await getHeaders(), sendTimeout: 5000));
      var responseMap = jsonDecode(response.toString());
      resultResponse = fromJson<T>(responseMap);
    } on DioError catch (e) {
      resultResponse = fromJson<T>(
          e?.response?.data.toString().contains('status')
              ? e?.response?.data
              : _defaultError);
    }

    return resultResponse;
  }

  /// GET LIST
  Future<List<T>> getList<T>(BuildContext context, String url) async {
    List<T> resultResponse;

    try {
      var response = await http.get(url);
      print('ResponseSuccess: ${response.toString()}');

      if (response.statusCode == 200) {
        final parsed = json.decode(response.body);
        resultResponse = _fromJsonList<T>(parsed);
      } else {
        resultResponse = List();
      }
    } on DioError catch (e) {
      print('ResponseError: ${e.response.toString()}');
      resultResponse = List();

      final statusCode = e?.response?.statusCode ?? 404;

      if (statusCode == 401) {
//        _logout(context);
      }
    }

    return resultResponse;
  }

  /// PUT
  Future<T> put<T>(BuildContext context, String url) async {
    T resultResponse;

    try {
      final dio = Dio();
      dio.interceptors.add(LogInterceptor(responseBody: true));
      dio.interceptors.add(RetryInterceptor(
        dio: dio,
        logger: Logger("Retry"),
        options: const RetryOptions(
            retryInterval: const Duration(seconds: 1), retries: 3),
      ));
      var response = await dio.put(url,
          options: Options(headers: await getHeaders(), sendTimeout: 5000));
      resultResponse = fromJson<T>(response.data);
    } on DioError catch (e) {
      final statusCode = e?.response?.statusCode ?? 404;

      if (statusCode == 401) {
//        _logout(context);
      } else {
        resultResponse = fromJson<T>(
            e?.response?.data.toString().contains('status')
                ? e?.response?.data
                : _defaultError);
      }
    }

    return resultResponse;
  }

  /// DELETE
  Future<T> delete<T>(BuildContext context, String url) async {
    T resultResponse;

    try {
      final dio = Dio();
      dio.interceptors.add(LogInterceptor(responseBody: true));
      var response = await dio.delete(url,
          options: Options(headers: await getHeaders(), sendTimeout: 5000));
      resultResponse = fromJson<T>(response.data);
    } on DioError catch (e) {
      final statusCode = e?.response?.statusCode ?? 404;

      if (statusCode == 401) {
//        _logout(context);
      } else {
        resultResponse = fromJson<T>(
            e?.response?.data.toString().contains('status')
                ? e?.response?.data
                : _defaultError);
      }
    }

    return resultResponse;
  }

  /// Register any networking model class here
  static T fromJson<T>(dynamic json) {
    if (json is Iterable) {
      return _fromJsonList<T>(json) as T;
    } else if (T == PokemonModel) {
      return PokemonModel.fromJson(json) as T;
    } else if (T == PokemonDetailModel) {
      return PokemonDetailModel.fromJson(json) as T;
    } else {
      /// if this print statement occured, this means that you're not register the model class in here
      throw Exception(
          'Unknown class, dont forget to add your model in BaseService.dart to parse the json');
    }
  }

  static List<T> _fromJsonList<T>(List jsonList) {
    if (jsonList == null) {
      return null;
    }

    List<T> output = List();

    for (Map<String, dynamic> json in jsonList) {
      output.add(fromJson(json));
    }

    return output;
  }
}
