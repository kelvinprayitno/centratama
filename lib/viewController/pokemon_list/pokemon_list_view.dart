import 'dart:io';
import 'dart:ui';

import 'package:centratamagroup/core/constant.dart';
import 'package:centratamagroup/core/locator.dart';
import 'package:centratamagroup/viewController/pokemon_list/pokemon_detail_argument.dart';
import 'package:centratamagroup/viewController/pokemon_list/stores/pokemon_list_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../shimmer_network_image.dart';

class PokemonListView extends StatefulWidget {
  @override
  _PokemonListViewState createState() => _PokemonListViewState();
}

class _PokemonListViewState extends State<PokemonListView>
    with SingleTickerProviderStateMixin {
  var store = locator<PokemonListStore>();

  ScrollController _scrollController = new ScrollController();

  AnimationController animationController;

  @override
  void initState() {
    animationController = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: 2),
    );

    animationController.repeat();

    store.init();

    store.getPokemonList();

    _scrollController
      ..addListener(() {
        debugPrint(
            "loadMoree ${_scrollController.position.pixels} ${_scrollController.position.maxScrollExtent}");
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          store.getPokemonList();
        }
      });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        brightness: Platform.isIOS ? Brightness.light : null,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Row(
          children: [
            Container(
              margin: const EdgeInsets.only(left: 20),
              child: Text(
                "Pokemon List",
                style: TextStyle(color: Colors.black, fontSize: 24),
              ),
            )
          ],
        ),
      ),
      body: Observer(builder: (_) {
        return SingleChildScrollView(
          controller: _scrollController,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              store.failed
                  ? GestureDetector(
                      child: Container(
                        child: Image.asset("assets/retry_ic.png"),
                      ),
                      onTap: () => store.getPokemonList(),
                    )
                  : SizedBox(
                      height: 0,
                    ),
              ListView.separated(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.only(bottom: 24),
                  itemBuilder: (context, position) {
                    return GestureDetector(
                      child: Expanded(
                        child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(6))),
                          margin: const EdgeInsets.only(left: 20, right: 20),
                          child: Container(
                            child: Column(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(right: 16),
                                  child: ShimmerNetworkImage(
                                    url:
                                        '${store.list[position].pokemonDetailModel?.sprites?.front_default ?? ""}',
                                    size: Size(60, 60),
                                    borderRadius: 1000,
                                    boxFit: BoxFit.cover,
                                  ),
                                ),
                                Container(
                                  child: Text(store.list[position].name,
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  margin: const EdgeInsets.only(right: 16),
                                )
                              ],
                            ),
                            margin: const EdgeInsets.all(10),
                          ),
                        ),
                      ),
                      onTap: () => Navigator.pushNamed(
                          context, Constant.POKEMON_DETAIL_ROUTE,
                          arguments: PokemonDetailArgument(
                              store.list[position].pokemonDetailModel)),
                    );
                  },
                  separatorBuilder: (context, position) => Divider(
                        color: Colors.transparent,
                        height: 10,
                      ),
                  itemCount: store.list?.length ?? 0),
              store.loadMore
                  ? RotationTransition(
                      turns: Tween(begin: 0.0, end: 1.0)
                          .animate(animationController),
                      child: Center(
                        child: Container(
                          child: Image.asset(
                            'assets/circle_loading.png',
                            height: 50.0,
                            width: 50.0,
                          ),
                        ),
                      ),
                    )
                  : SizedBox(
                      height: 0,
                    )
            ],
          ),
        );
      }),
    );
  }
}
