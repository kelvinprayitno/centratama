import 'package:centratamagroup/core/constant.dart';
import 'package:centratamagroup/model/pokemon_model.dart';
import 'package:centratamagroup/networking/service/user_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';

part 'pokemon_list_store.g.dart';

class PokemonListStore = _PokemonListStore with _$PokemonListStore;

abstract class _PokemonListStore with Store {

  @observable
  bool failed;

  @observable
  bool loadMore;

  int offset;
  int limit;

  @observable
  PokemonModel data;

  @observable
  ObservableList<PokemonModelData> list = ObservableList.of([]);

  @action
  init(){
    failed = false;
    data = null;
    loadMore = false;
    list = ObservableList.of([]);
}

  @action
  getPokemonList() async{

    loadMore = true;

    final _data = await UserService().getPokemonList(data?.next ?? "${Constant.BASE_API}pokemon?limit=15&offset=0");

    if (_data != null) {
      data = _data;
      loadMore = false;

      if(_data.results.length > 0){
        list.addAll(_data.results);

        for(int c=0; c < _data.results.length ; c++){
          getPokemonData(c);
        }
      }
    } else {
      loadMore = false;
      // fail get data
      if(data == null)
      failed = true;
    }
  }

  @action
  getPokemonData(int index) async{

    final _data = await UserService().getPokemonDetail(data.results[index].name);

    if (_data != null) {
      data.results[index].pokemonDetailModel = _data;
    } else {
      // fail get data
      failed = true;
    }
  }

  @action
  loadMoree() async{
    loadMore = true;
  }
}