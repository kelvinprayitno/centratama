// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon_list_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PokemonListStore on _PokemonListStore, Store {
  final _$failedAtom = Atom(name: '_PokemonListStore.failed');

  @override
  bool get failed {
    _$failedAtom.reportRead();
    return super.failed;
  }

  @override
  set failed(bool value) {
    _$failedAtom.reportWrite(value, super.failed, () {
      super.failed = value;
    });
  }

  final _$loadMoreAtom = Atom(name: '_PokemonListStore.loadMore');

  @override
  bool get loadMore {
    _$loadMoreAtom.reportRead();
    return super.loadMore;
  }

  @override
  set loadMore(bool value) {
    _$loadMoreAtom.reportWrite(value, super.loadMore, () {
      super.loadMore = value;
    });
  }

  final _$dataAtom = Atom(name: '_PokemonListStore.data');

  @override
  PokemonModel get data {
    _$dataAtom.reportRead();
    return super.data;
  }

  @override
  set data(PokemonModel value) {
    _$dataAtom.reportWrite(value, super.data, () {
      super.data = value;
    });
  }

  final _$listAtom = Atom(name: '_PokemonListStore.list');

  @override
  ObservableList<PokemonModelData> get list {
    _$listAtom.reportRead();
    return super.list;
  }

  @override
  set list(ObservableList<PokemonModelData> value) {
    _$listAtom.reportWrite(value, super.list, () {
      super.list = value;
    });
  }

  final _$getPokemonListAsyncAction =
      AsyncAction('_PokemonListStore.getPokemonList');

  @override
  Future getPokemonList() {
    return _$getPokemonListAsyncAction.run(() => super.getPokemonList());
  }

  final _$getPokemonDataAsyncAction =
      AsyncAction('_PokemonListStore.getPokemonData');

  @override
  Future getPokemonData(int index) {
    return _$getPokemonDataAsyncAction.run(() => super.getPokemonData(index));
  }

  final _$loadMoreeAsyncAction = AsyncAction('_PokemonListStore.loadMoree');

  @override
  Future loadMoree() {
    return _$loadMoreeAsyncAction.run(() => super.loadMoree());
  }

  final _$_PokemonListStoreActionController =
      ActionController(name: '_PokemonListStore');

  @override
  dynamic init() {
    final _$actionInfo = _$_PokemonListStoreActionController.startAction(
        name: '_PokemonListStore.init');
    try {
      return super.init();
    } finally {
      _$_PokemonListStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
failed: ${failed},
loadMore: ${loadMore},
data: ${data},
list: ${list}
    ''';
  }
}
