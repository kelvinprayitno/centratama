import 'package:centratamagroup/model/pokemon_detail_model.dart';

class PokemonDetailArgument{

  PokemonDetailArgument(this.pokemonDetailModel);

  PokemonDetailModel pokemonDetailModel;
}