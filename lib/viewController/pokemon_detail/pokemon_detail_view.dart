import 'dart:io';

import 'package:centratamagroup/model/pokemon_detail_model.dart';
import 'package:centratamagroup/viewController/pokemon_detail/pokemon_detail_bloc.dart';
import 'package:centratamagroup/viewController/pokemon_list/pokemon_detail_argument.dart';
import 'package:flutter/material.dart';

import '../shimmer_network_image.dart';

class PokemonDetailView extends StatefulWidget {
  final PokemonDetailArgument pokemonData;

  const PokemonDetailView({Key key, this.pokemonData}) : super(key: key);

  @override
  _PokemonDetailViewState createState() => _PokemonDetailViewState();
}

class _PokemonDetailViewState extends State<PokemonDetailView> {
  final bloc = PokemonDetailBloc();

  @override
  void initState() {
    super.initState();

    bloc.setupData(widget.pokemonData.pokemonDetailModel);
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Platform.isIOS ? Brightness.light : null,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        leading: BackButton(color: Colors.black),
        title: Text(
          "Pokemon List",
          style: TextStyle(color: Colors.black, fontSize: 24),
        ),
        centerTitle: true,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: StreamBuilder<PokemonDetailModel>(
            stream: bloc.pokemonStream,
            builder: (context, snapshot) {
              return snapshot.hasData
                  ? Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(right: 16),
                              child: ShimmerNetworkImage(
                                url:
                                    '${snapshot.data.sprites.front_default ?? ""}',
                                size: Size(100, 100),
                                borderRadius: 1000,
                                boxFit: BoxFit.cover,
                              ),
                            ),
                            Container(
                              child: ShimmerNetworkImage(
                                url:
                                    '${snapshot.data.sprites.back_default ?? ""}',
                                size: Size(60, 60),
                                borderRadius: 1000,
                                boxFit: BoxFit.cover,
                              ),
                            )
                          ],
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 10),
                          child: Text("Name: ${snapshot.data.name}",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16)),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 10),
                          child: Text("Weight: ${snapshot.data.weight}",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16)),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 10),
                          child: Text("Height: ${snapshot.data.height}",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16)),
                        ),
                        ListView.separated(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            padding: const EdgeInsets.only(bottom: 24, top: 30),
                            itemBuilder: (context, position) {
                              var item = snapshot.data.stats[position];
                              return Column(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.only(top: 10),
                                    child: Text(
                                        "${item.stat.name}: ${item.base_stat}",
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 16)),
                                  )
                                ],
                              );
                            },
                            separatorBuilder: (context, position) => Divider(
                                  color: Colors.transparent,
                                  height: 5,
                                ),
                            itemCount: snapshot.data.stats.length ?? 0)
                      ],
                    )
                  : SizedBox(
                      height: 0,
                    );
            },
          ),
        ),
      ),
    );
  }
}
