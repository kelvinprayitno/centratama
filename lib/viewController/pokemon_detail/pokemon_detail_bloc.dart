import 'package:bloc_provider/bloc_provider.dart';
import 'package:centratamagroup/model/pokemon_detail_model.dart';
import 'package:centratamagroup/networking/service/user_service.dart';
import 'package:rxdart/rxdart.dart';

class PokemonDetailBloc{

  dispose() {
    pokemonSubject.close();
  }

  final pokemonSubject = PublishSubject<PokemonDetailModel>();

  Stream<PokemonDetailModel> get pokemonStream =>
      pokemonSubject.stream;

  setupData(PokemonDetailModel data) {
    getPokemonDetail(data);
  }

  getPokemonDetail(PokemonDetailModel data) async {
    final _data = await UserService().getPokemonDetail(data.name);

    if (_data != null) {
      pokemonSubject.sink.add(_data);
    } else {
      // fail get data
//      failed = true;
    }
  }
}

final pokemonDetailBloc = PokemonDetailBloc();

