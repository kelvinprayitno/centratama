import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ContainerShimmer extends StatelessWidget {

  ContainerShimmer({Key key, @required this.size, @required this.borderRadius}): super (key: key);

  final Size size;
  final double borderRadius;

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Color(0xFFF2F2F2),
      highlightColor: Colors.white,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
          color: Color(0xFFF2F2F2),
        ),
        height: size.height,
        width: size.width,
      ),
    );
  }
}
