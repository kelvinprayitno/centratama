import 'package:json_annotation/json_annotation.dart';

part 'pokemon_detail_model.g.dart';

@JsonSerializable()
class PokemonDetailModel{

  PokemonDetailModel(this.name, this.height, this.abilities, this.sprites, this.weight);

  @JsonKey(name: "weight")
  int weight;
  @JsonKey(name: "height")
  int height;
  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "abilities")
  List<AbilityData> abilities;
  @JsonKey(name: "stats")
  List<StatsData> stats;
  @JsonKey(name: "sprites")
  SpriteData sprites;

  factory PokemonDetailModel.fromJson(Map<String, dynamic> json) => _$PokemonDetailModelFromJson(json);
  Map<String, dynamic> toJson() => _$PokemonDetailModelToJson(this);
}

@JsonSerializable()
class AbilityData{

  AbilityData(this.ability);

  @JsonKey(name: "ability")
  Ability ability;

  factory AbilityData.fromJson(Map<String, dynamic> json) => _$AbilityDataFromJson(json);
  Map<String, dynamic> toJson() => _$AbilityDataToJson(this);
}

@JsonSerializable()
class Ability{

  Ability(this.name, this.url);

  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "url")
  String url;

  factory Ability.fromJson(Map<String, dynamic> json) => _$AbilityFromJson(json);
  Map<String, dynamic> toJson() => _$AbilityToJson(this);
}

@JsonSerializable()
class SpriteData{

  SpriteData(this.back_default, this.back_female, this.front_default, this.front_female);

  @JsonKey(name: "back_default")
  String back_default;
  @JsonKey(name: "back_female")
  String back_female;
  @JsonKey(name: "front_default")
  String front_default;
  @JsonKey(name: "front_female")
  String front_female;

  factory SpriteData.fromJson(Map<String, dynamic> json) => _$SpriteDataFromJson(json);
  Map<String, dynamic> toJson() => _$SpriteDataToJson(this);
}

@JsonSerializable()
class StatsData{

  StatsData(this.base_stat, this.effort, this.stat);

  @JsonKey(name: "base_stat")
  int base_stat;
  @JsonKey(name: "effort")
  int effort;
  @JsonKey(name: "stat")
  Stats stat;

  factory StatsData.fromJson(Map<String, dynamic> json) => _$StatsDataFromJson(json);
  Map<String, dynamic> toJson() => _$StatsDataToJson(this);
}

@JsonSerializable()
class Stats{

  Stats(this.name, this.url);

  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "url")
  String url;

  factory Stats.fromJson(Map<String, dynamic> json) => _$StatsFromJson(json);
  Map<String, dynamic> toJson() => _$StatsToJson(this);
}