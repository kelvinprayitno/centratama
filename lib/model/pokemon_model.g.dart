// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PokemonModel _$PokemonModelFromJson(Map<String, dynamic> json) {
  return PokemonModel(
    json['count'] as int,
    json['next'] as String,
    json['previous'] as String,
    (json['results'] as List)
        ?.map((e) => e == null
            ? null
            : PokemonModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$PokemonModelToJson(PokemonModel instance) =>
    <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results,
    };

PokemonModelData _$PokemonModelDataFromJson(Map<String, dynamic> json) {
  return PokemonModelData(
    json['name'] as String,
    json['url'] as String,
  )..pokemonDetailModel = json['pokemonDetailModel'] == null
      ? null
      : PokemonDetailModel.fromJson(
          json['pokemonDetailModel'] as Map<String, dynamic>);
}

Map<String, dynamic> _$PokemonModelDataToJson(PokemonModelData instance) =>
    <String, dynamic>{
      'name': instance.name,
      'url': instance.url,
      'pokemonDetailModel': instance.pokemonDetailModel,
    };
