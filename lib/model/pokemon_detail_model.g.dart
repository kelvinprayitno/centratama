// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PokemonDetailModel _$PokemonDetailModelFromJson(Map<String, dynamic> json) {
  return PokemonDetailModel(
    json['name'] as String,
    json['height'] as int,
    (json['abilities'] as List)
        ?.map((e) =>
            e == null ? null : AbilityData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['sprites'] == null
        ? null
        : SpriteData.fromJson(json['sprites'] as Map<String, dynamic>),
    json['weight'] as int,
  )..stats = (json['stats'] as List)
      ?.map((e) =>
          e == null ? null : StatsData.fromJson(e as Map<String, dynamic>))
      ?.toList();
}

Map<String, dynamic> _$PokemonDetailModelToJson(PokemonDetailModel instance) =>
    <String, dynamic>{
      'weight': instance.weight,
      'height': instance.height,
      'name': instance.name,
      'abilities': instance.abilities,
      'stats': instance.stats,
      'sprites': instance.sprites,
    };

AbilityData _$AbilityDataFromJson(Map<String, dynamic> json) {
  return AbilityData(
    json['ability'] == null
        ? null
        : Ability.fromJson(json['ability'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AbilityDataToJson(AbilityData instance) =>
    <String, dynamic>{
      'ability': instance.ability,
    };

Ability _$AbilityFromJson(Map<String, dynamic> json) {
  return Ability(
    json['name'] as String,
    json['url'] as String,
  );
}

Map<String, dynamic> _$AbilityToJson(Ability instance) => <String, dynamic>{
      'name': instance.name,
      'url': instance.url,
    };

SpriteData _$SpriteDataFromJson(Map<String, dynamic> json) {
  return SpriteData(
    json['back_default'] as String,
    json['back_female'] as String,
    json['front_default'] as String,
    json['front_female'] as String,
  );
}

Map<String, dynamic> _$SpriteDataToJson(SpriteData instance) =>
    <String, dynamic>{
      'back_default': instance.back_default,
      'back_female': instance.back_female,
      'front_default': instance.front_default,
      'front_female': instance.front_female,
    };

StatsData _$StatsDataFromJson(Map<String, dynamic> json) {
  return StatsData(
    json['base_stat'] as int,
    json['effort'] as int,
    json['stat'] == null
        ? null
        : Stats.fromJson(json['stat'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$StatsDataToJson(StatsData instance) => <String, dynamic>{
      'base_stat': instance.base_stat,
      'effort': instance.effort,
      'stat': instance.stat,
    };

Stats _$StatsFromJson(Map<String, dynamic> json) {
  return Stats(
    json['name'] as String,
    json['url'] as String,
  );
}

Map<String, dynamic> _$StatsToJson(Stats instance) => <String, dynamic>{
      'name': instance.name,
      'url': instance.url,
    };
