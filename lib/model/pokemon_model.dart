import 'package:centratamagroup/model/pokemon_detail_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'pokemon_model.g.dart';

@JsonSerializable()
class PokemonModel{
  PokemonModel(this.count, this.next, this.previous, this.results);

  @JsonKey(name: "count")
  int count;
  @JsonKey(name: "next")
  String next;
  @JsonKey(name: "previous")
  String previous;
  @JsonKey(name: "results")
  List<PokemonModelData> results;

  factory PokemonModel.fromJson(Map<String, dynamic> json) => _$PokemonModelFromJson(json);
  Map<String, dynamic> toJson() => _$PokemonModelToJson(this);
}

@JsonSerializable()
class PokemonModelData{
  PokemonModelData(this.name, this.url);

  String name;
  String url;
  PokemonDetailModel pokemonDetailModel;

  factory PokemonModelData.fromJson(Map<String, dynamic> json) => _$PokemonModelDataFromJson(json);
  Map<String, dynamic> toJson() => _$PokemonModelDataToJson(this);
}
