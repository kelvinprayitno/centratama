class Constant {

  static const SUCCESS = 'SUCCESS';
  static const ERROR = 'ERROR';
  static const ERROR_MESSAGE = 'Gateway error, please try again.';
  static const API_PRODUCTION = 'api_production';
  static const BASE_API = 'https://pokeapi.co/api/v2/';

  // SHARED PREFERENCE //
  static const TOKEN = 'TOKEN';

  // ERROR //
  static const NETWORK_ERROR = 'Network error, please try again later.';

//  router
  static const POKEMON_LIST_ROUTE = 'pokemon_list_route';
  static const POKEMON_DETAIL_ROUTE = 'pokemon_detail_route';
}
