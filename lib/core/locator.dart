import 'package:centratamagroup/viewController/pokemon_list/stores/pokemon_list_store.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.I;

void setupLocator() {
  locator.registerLazySingleton<PokemonListStore>(() => PokemonListStore());

}
