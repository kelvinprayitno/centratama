import 'package:centratamagroup/core/constant.dart';
import 'package:centratamagroup/viewController/pokemon_detail/pokemon_detail_view.dart';
import 'package:centratamagroup/viewController/pokemon_list/pokemon_list_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Router {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Constant.POKEMON_LIST_ROUTE:
        return MaterialPageRoute(builder: (_) => PokemonListView());
      case Constant.POKEMON_DETAIL_ROUTE:
        return MaterialPageRoute(builder: (_) => PokemonDetailView(
          pokemonData: settings.arguments,
        ));
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
