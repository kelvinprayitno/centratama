import 'package:centratamagroup/core/constant.dart';
import 'package:flutter/material.dart';

import 'core/locator.dart';
import 'core/router.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

final globalNavigatorKey = GlobalKey<NavigatorState>();

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Centratama Group',
      navigatorKey: globalNavigatorKey,
      initialRoute: Constant.POKEMON_LIST_ROUTE,
      onGenerateRoute: Router.generateRoute,
    );
  }
}

